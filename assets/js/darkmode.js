// DarkMode and cookie status

function setCookie(cname,cvalue,exdays) {
  const d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  let expires = "expires=" + d.toGMTString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  let name = cname + "=";
  let decodedCookie = decodeURIComponent(document.cookie);
  let ca = decodedCookie.split(';');
  for(let i = 0; i < ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function delCookie(cname) {
  const d = new Date();
  d.setTime(d.getTime() - (100*24*60*60*1000));
  let expires = "expires=" + d.toGMTString();
  document.cookie = cname + "=" + null + ";" + expires + ";path=/";
}


$(function() {
  if (getCookie('dark') != "") {
    $("body").toggleClass("page-dark-mode");
    BeautifulJekyllJS.initNavbar();
  }
  $('#change-skin').on('click', function () {
    if ($("body").hasClass("page-dark-mode")) { delCookie('dark'); }
    else { setCookie('dark', true, 30); }
    $("body").toggleClass("page-dark-mode");
    BeautifulJekyllJS.initNavbar();
  });
});
